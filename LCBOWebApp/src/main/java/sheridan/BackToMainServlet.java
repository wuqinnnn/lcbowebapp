package sheridan;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
        name = "backtomainservlet",
        urlPatterns = "/BackToMain"
)

public class BackToMainServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1831978487671842618L;
	
    
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	RequestDispatcher view = req.getRequestDispatcher("index.html");
    	view.forward(req, resp);
    }

}
